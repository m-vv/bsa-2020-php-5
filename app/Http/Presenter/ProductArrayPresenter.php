<?php

declare(strict_types=1);

namespace App\Http\Presenter;

use App\Entity\Product;
use ReflectionClass;

class ProductArrayPresenter
{
    /**
     * @param Product[] $products
     * @return array
     */
    public static function presentCollection(array $products): array
    {
        $res = [];
        foreach ($products as $product) {
            array_push($res, self::present($product));
        }
        return $res;
    }

    public static function present(Product $product): array
    {
        $reflectionClass = new ReflectionClass(get_class($product));
        $array = array();
        foreach ($reflectionClass->getProperties() as $property) {
            $property->setAccessible(true);
            $array[$property->getName()] = $property->getValue($product);
            $property->setAccessible(false);
        }
        $array['price'] = $product->getPrice();
        return $array;
    }
}
