<?php

declare(strict_types=1);

namespace App\Entity;

class Product
{
    private $id;
    private $name;
    private $price; //int for storing price
    private $img;
    private $rating;


    public function __construct(int $id, string $name, float $price, string $imageUrl, float $rating)
    {
        $this->id  = $id;
        $this->name  = $name;
        $this->name  = $name;
        $this->setPrice($price);
        $this->img = $imageUrl;
        $this->rating = $rating;
    }


    public function getId() : int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getPrice(): float
    {
        return round($this->price/100, 2);
    }

    public function setPrice(float $price): void
    {
        $this->price = intval($price*100);
    }

    public function getImageUrl()
    {
        return $this->img;
    }

    public function setImageUrl(string  $imageUrl): void
    {
        $this->img = $imageUrl;
    }

    public function getRating(): float
    {
        return $this->rating;
    }

    public function setRating(float $rating): void
    {
        $this->rating = $rating;
    }
}

