<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Entity\Product;

class GetCheapestProductsAction extends GetAllProductsAction
{
    public function execute()
    {
        usort($this->products, function (Product $a, Product $b) {
            return ($a->getPrice() <=> $b->getPrice());
        });
        return new GetAllProductsResponse(array_slice($this->products, 0, 3));
    }
}
