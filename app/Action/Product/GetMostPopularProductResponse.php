<?php

declare(strict_types=1);

namespace App\Action\Product;

class GetMostPopularProductResponse extends GetAllProductsResponse
{
    private $product;

    public function __construct(array $products)
    {
        $this->product = $products[0];
    }

    public function getProduct()
    {
        return $this->product;
    }
}
