<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Entity\Product;

class GetMostPopularProductAction extends GetAllProductsAction
{
    public function execute() // GetMostPopularProductResponse
    {
        usort($this->products, function (Product $a, Product $b) {
            return -($a->getRating() <=> $b->getRating());
        });

        return new GetMostPopularProductResponse([$this->products[0]]);
    }
}
