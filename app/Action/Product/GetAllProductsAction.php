<?php

declare(strict_types=1);

namespace App\Action\Product;


class GetAllProductsAction
{
    protected $products;

    public function __construct()
    {
        $productRepository = app('AllProd');
        $this->products = $productRepository->findAll();
    }

    public function execute() //GetAllProductsResponse
    {
        return new GetAllProductsResponse($this->products);
    }
}
