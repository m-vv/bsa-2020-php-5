<?php

use App\Action\Product\GetAllProductsAction;
use App\Http\Presenter\ProductArrayPresenter;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get(
    '/products',
    function () {
        $products = new GetAllProductsAction();
        $products = ProductArrayPresenter::presentCollection($products->execute()->getProducts());
        return response()->json($products);
    }
);

Route::get(
    '/products/popular',
    function () {
        $product = new \App\Action\Product\GetMostPopularProductAction();
        $product = ProductArrayPresenter::present($product->execute()->getProduct());
        return response()->json($product);
    }
);
