<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Action\Product\GetAllProductsAction;
use App\Http\Presenter\ProductArrayPresenter;
use App\Repository\ProductRepositoryInterface;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/products/cheap', function () {
    $products = new \App\Action\Product\GetCheapestProductsAction();
    $products = ProductArrayPresenter::presentCollection($products->execute()->getProducts());
    return view('cheap_products', [ 'products'=>$products]);
});
